// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef MYPROJECT_MyProjectGameMode_generated_h
#error "MyProjectGameMode.generated.h already included, missing '#pragma once' in MyProjectGameMode.h"
#endif
#define MYPROJECT_MyProjectGameMode_generated_h

#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_SPARSE_DATA
#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyProjectGameMode(); \
	friend struct Z_Construct_UClass_AMyProjectGameMode_Statics; \
public: \
	DECLARE_CLASS(AMyProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), MYPROJECT_API) \
	DECLARE_SERIALIZER(AMyProjectGameMode)


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAMyProjectGameMode(); \
	friend struct Z_Construct_UClass_AMyProjectGameMode_Statics; \
public: \
	DECLARE_CLASS(AMyProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), MYPROJECT_API) \
	DECLARE_SERIALIZER(AMyProjectGameMode)


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MYPROJECT_API AMyProjectGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProjectGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYPROJECT_API, AMyProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProjectGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYPROJECT_API AMyProjectGameMode(AMyProjectGameMode&&); \
	MYPROJECT_API AMyProjectGameMode(const AMyProjectGameMode&); \
public:


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYPROJECT_API AMyProjectGameMode(AMyProjectGameMode&&); \
	MYPROJECT_API AMyProjectGameMode(const AMyProjectGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYPROJECT_API, AMyProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProjectGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyProjectGameMode)


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_PRIVATE_PROPERTY_OFFSET
#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_19_PROLOG
#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_RPC_WRAPPERS \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_INCLASS \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_INCLASS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_MyProjectGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class AMyProjectGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_4_24_Source_MyProject_MyProjectGameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) \
	op(EGamePlayState::EUnknown) 

enum class EGamePlayState;
template<> MYPROJECT_API UEnum* StaticEnum<EGamePlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
