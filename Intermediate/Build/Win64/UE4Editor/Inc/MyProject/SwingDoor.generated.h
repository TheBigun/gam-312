// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef MYPROJECT_SwingDoor_generated_h
#error "SwingDoor.generated.h already included, missing '#pragma once' in SwingDoor.h"
#endif
#define MYPROJECT_SwingDoor_generated_h

#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_SPARSE_DATA
#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleDoor) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_ForwardVector); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleDoor(Z_Param_ForwardVector); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCloseDoor) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dt); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CloseDoor(Z_Param_dt); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOpenDoor) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dt); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OpenDoor(Z_Param_dt); \
		P_NATIVE_END; \
	}


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleDoor) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_ForwardVector); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleDoor(Z_Param_ForwardVector); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCloseDoor) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dt); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CloseDoor(Z_Param_dt); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOpenDoor) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dt); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OpenDoor(Z_Param_dt); \
		P_NATIVE_END; \
	}


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASwingDoor(); \
	friend struct Z_Construct_UClass_ASwingDoor_Statics; \
public: \
	DECLARE_CLASS(ASwingDoor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASwingDoor)


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASwingDoor(); \
	friend struct Z_Construct_UClass_ASwingDoor_Statics; \
public: \
	DECLARE_CLASS(ASwingDoor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASwingDoor)


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASwingDoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASwingDoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASwingDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASwingDoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASwingDoor(ASwingDoor&&); \
	NO_API ASwingDoor(const ASwingDoor&); \
public:


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASwingDoor(ASwingDoor&&); \
	NO_API ASwingDoor(const ASwingDoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASwingDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASwingDoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASwingDoor)


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_312_4_24_Source_MyProject_SwingDoor_h_9_PROLOG
#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_RPC_WRAPPERS \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_INCLASS \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_4_24_Source_MyProject_SwingDoor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_INCLASS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_SwingDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class ASwingDoor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_4_24_Source_MyProject_SwingDoor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
