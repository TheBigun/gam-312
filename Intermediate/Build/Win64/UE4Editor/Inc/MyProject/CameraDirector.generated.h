// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_CameraDirector_generated_h
#error "CameraDirector.generated.h already included, missing '#pragma once' in CameraDirector.h"
#endif
#define MYPROJECT_CameraDirector_generated_h

#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_SPARSE_DATA
#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_RPC_WRAPPERS
#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraDirector(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraDirector) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public:


#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraDirector)


#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_312_4_24_Source_MyProject_CameraDirector_h_9_PROLOG
#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_RPC_WRAPPERS \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_INCLASS \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_4_24_Source_MyProject_CameraDirector_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_INCLASS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_CameraDirector_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class ACameraDirector>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_4_24_Source_MyProject_CameraDirector_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
