// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_OpenDoor_generated_h
#error "OpenDoor.generated.h already included, missing '#pragma once' in OpenDoor.h"
#endif
#define MYPROJECT_OpenDoor_generated_h

#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_SPARSE_DATA
#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_RPC_WRAPPERS
#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenDoor(); \
	friend struct Z_Construct_UClass_AOpenDoor_Statics; \
public: \
	DECLARE_CLASS(AOpenDoor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AOpenDoor)


#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOpenDoor(); \
	friend struct Z_Construct_UClass_AOpenDoor_Statics; \
public: \
	DECLARE_CLASS(AOpenDoor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AOpenDoor)


#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOpenDoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenDoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenDoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenDoor(AOpenDoor&&); \
	NO_API AOpenDoor(const AOpenDoor&); \
public:


#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenDoor(AOpenDoor&&); \
	NO_API AOpenDoor(const AOpenDoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenDoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenDoor)


#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_312_4_24_Source_MyProject_OpenDoor_h_9_PROLOG
#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_RPC_WRAPPERS \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_INCLASS \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_4_24_Source_MyProject_OpenDoor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_SPARSE_DATA \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_INCLASS_NO_PURE_DECLS \
	gam_312_4_24_Source_MyProject_OpenDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class AOpenDoor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_4_24_Source_MyProject_OpenDoor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
