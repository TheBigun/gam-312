// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyProjectHUD.generated.h"

UCLASS()
class AMyProjectHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMyProjectHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	//HUDwidget asset pointer
	UPROPERTY(EditAnywhere, Category = "Health")
		TSubclassOf<class UUserWidget> HUDWidgetClass;

	//Current widget asset pointer
	UPROPERTY(EditAnywhere, Category = "Health")
		class UUserWidget* CurrentWidget;

};

