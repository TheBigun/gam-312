// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "CameraDirector.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
    Super::BeginPlay();

    cameraChange = false;

    UE_LOG(LogTemp, Warning, TEXT("Camera Director BeginPlay"));
}

// Called every frame
// This Version includes logic to make the camera toggle from first person perspective to the new actor.

void ACameraDirector::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );

    const float TimeBetweenCameraChanges = 2.0f;
    const float SmoothBlendTime = 0.75f;
    TimeToNextCameraChange -= DeltaTime;

    if (cameraChange)
    {
        if (TimeToNextCameraChange <= 0.0f)
        {
            TimeToNextCameraChange += TimeBetweenCameraChanges;

            //Find the actor that handles control for the local player.
            APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
            if (OurPlayerController)
            {
                // if else to determine which camera the player is viewing and switch to the opposite when button is pressed
                if (CameraTwo && (OurPlayerController->GetViewTarget() == CameraOne))
                {
                    OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);
                }
                else if (CameraOne)
                {
                    OurPlayerController->SetViewTargetWithBlend(CameraOne, SmoothBlendTime);
                }
            }
        }
        else
        {
            cameraChange = false;
            TimeToNextCameraChange = 0.0f;
        }
    }
   
}

void ACameraDirector::cameraGo()
{
    // activates the camera change
    cameraChange = true;
    // resets number to zero
    TimeToNextCameraChange = 0.0f;
    // UE_LOG(LogTemp, Warning, TEXT("Pressed C Button"));

    UE_LOG(LogTemp, Warning, TEXT("cameraGo function activated"));
}
