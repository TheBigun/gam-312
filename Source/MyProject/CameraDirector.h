// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraDirector.generated.h"

UCLASS()
class MYPROJECT_API ACameraDirector : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ACameraDirector();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaSeconds) override;

    UPROPERTY(EditAnywhere)
        AActor* CameraOne;

    UPROPERTY(EditAnywhere)
        AActor* CameraTwo;

    // Input FUnctions
    void cameraGo();

    // Input Variables
    float TimeToNextCameraChange;
    bool cameraChange;
};